Name:           userspace-rcu
Version:        0.14.0
Release:        3
Summary:        Userspace read-copy-update library
License:        LGPLv2+
URL:            http://liburcu.org
Source0:        http://lttng.org/files/urcu/%{name}-%{version}.tar.bz2
Patch0001:      userspace-rcu-0.13.0-sw.patch
Patch0002:      add-loongarch.patch
Patch0003:      userspace-rcu-0.14.0-fix-conditional-compilation-for-LoongArch-architectu.patch
BuildRequires:  pkgconfig gcc gcc-c++ autoconf automake make libtool

%description
liburcu is a LGPLv2.1 userspace RCU (read-copy-update) library. This data
synchronization library provides read-side access which scales linearly with
the number of cores. It does so by allowing multiples copies of a given data
structure to live at the same time, and by monitoring the data structure
accesses to detect grace periods after which memory reclamation is possible.

%package        devel
Summary:        The devel for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Development files for %{name}

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif

%configure 

make %{?_smp_mflags} V=1


%install
%make_install


%check
make check
#make regtest

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%license LICENSE gpl-2.0.txt lgpl-relicensing.txt lgpl-2.1.txt
%doc ChangeLog README.md
%{_libdir}/*.so.*
%exclude %{_libdir}/*.a
%exclude %{_libdir}/*.la
%exclude %{_docdir}/%{name}/LICENSE

%files devel
%doc %{_pkgdocdir}/examples
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/liburcu*.pc
%{_docdir}/%{name}/cds-api.md
%{_docdir}/%{name}/rcu-api.md
%{_docdir}/%{name}/solaris-build.md
%{_docdir}/%{name}/uatomic-api.md


%changelog
* Wed Feb 12 2025 yueyuankun <yueyuankun@kylinos.cn> - 0.14.0-3
- Type: compile
- ID: NA
- SUG: NA
- DESC: Fix compilation errors in loongarch architecture
        Replace setup with autosetup

* Thu Dec 19 2024 wangxiao <wangxiao184@h-partners.com> - 0.14.0-2
- include all patches in the source package

* Wed Jan 24 2024 liuchao <liuchao173@huawei.com> - 0.14.0-1
- upgrade to 0.14.0:
  - Fix: urcu-bp: only teardown call-rcu worker in destructor
  - Fix: rculfhash: urcu_die() takes positive error value
  - Fix: call_rcu: teardown default call_rcu worker on application exit
  - Fix: join worker thread in call_rcu_data_free
  - Docs: clarify grace period polling API
  - Document grace period polling in rcu-api.md
  - Implement poll rcu stress test in rcutorture
  - urcu-memb,mb,signal: Implement grace period polling
  - Fix: auto-resize hash table destroy deadlock
  - Fix building on MSYS2
  - rculfhash: Include rculfhash-internal.h from local directory
  - Remove "Darwin" from "should also work on list"
  - Merge branch 'adah1972-improve-md'
  - Add semicolons at the end of function prototypes
  - Wrap a file name in backticks
  - Wrap command-line options in backticks
  - Fix a wrong format
  - Wrap URLs in angle brackets
  - Fix Markdown issues
  - Fix: Always check pthread_create for failures
  - Disable signals in URCU background threads
  - Fix: futex.h: include headers outside extern C
  - Fix: add missing unused attribute to _rcu_dereference
  - Fix: change method used by _rcu_dereference to strip type constness
  - Fix: remove type constness in URCU_FORCE_CAST's C++ version
  - Move extern "C" down in include/urcu/urcu-bp.h
  - fix: ifdef linux specific cpu count compat
  - Add unit tests for possible_cpus_array_len
  - fix: sysconf(_SC_NPROCESSORS_CONF) can be less than max cpu id
  - Fix: revise obsolete command in README.md
  - Fix: workqueue: remove unused variable "ret"
  - Fix: urcu-qsbr: futex wait: handle spurious futex wakeups
  - Fix: urcu: futex wait: handle spurious futex wakeups
  - Fix: urcu-wait: futex wait: handle spurious futex wakeups
  - Fix: defer_rcu: futex wait: handle spurious futex wakeups
  - Fix: call_rcu: futex wait: handle spurious futex wakeups
  - Fix: workqueue: futex wait: handle spurious futex wakeups
  - Fix: Use %lu rather than %ld to print count
  - Update ABI definition files
  - Bump version current and age
  - alpha: allocate membarrier system call number
  - Bump version to 0.14.0-pre
  - Improved test framework
  - rculfhash: introduce cds_lfht_node_init_deleted
  - Fix: changelog: v0.13.0 was released in 2021
  - cleanup: i386 arch detection
  - fix: properly detect 'cmpxchg' on x86-32
  - fix: use urcu-tls compat with c++ compiler
  - Fix typo
  - fix: remove autoconf features default value in help message
  - fix: add missing pkgconfig file for memb flavour lib
  - Cleanup: Tests: Remove useless pre-C99 compatibility code from tap.h
  - Document C99 and C++11 requirement in README.md
  - Always use '__thread' for Thread local storage except on MSVC
  - Fix: powerpc32: transparent unions alter calling convention
  - fix: don't use C++ thread_local on MacOs
  - wfcqueue API: implement overloaded wrappers with templates
  - wfcqueue: combine C++ API cds_wfcq_head_cast with overloading
  - wfstack C++ API: implement overloaded wrappers with templates
  - lfstack C++ API: implement overloaded wrappers with templates
  - wfstack: combine C++ API cds_wfs_stack_cast with overloading
  - lfstack: combine C++ API cds_lfs_stack_cast with overloading
  - fix: test_build tap plan
  - Test C++ build of list head init
  - Fix order of initializers in CDS_LIST_HEAD_INIT
  - unit tests: test wfcqueue, wfstack, lfstack empty check functions in C++
  - wfcqueue: implement C++ API based on function overloading
  - wfstack: implement C++ API based on function overloading
  - lfstack: implement C++ API based on function overloading
  - Fix tap.h: remove extra semicolon in pass/fail macros
  - Add C++ build tests
  - Build and run regression and unit tests as C++ programs
  - Add --enable-Werror configure switch
  - Add `urcu_posix_assert()` as `assert()` replacement
  - Rename `urcu_assert()` to `urcu_assert_debug()`
  - cleanup: spelling fixes in comments
  - Make temporary variable in _rcu_dereference non-const
  - (tls-ie2) Fix: x86 and s390: uatomic __hp() macro C++ support
  - Fix: x86 and s390: uatomic __hp() macro clang support
  - Fix: x86 and s390 uatomic: __hp() macro warning with gcc 11

* Thu Feb 9 2023 laihangliang<laihangliang1@huawei.com> - 0.13.2-1
- upgrade to 0.13.2

* Mon Nov 14 2022 huajingyun<huajingyun@loongson.cn> - 0.13.0-5
- Add support loongarch

* Thu Aug 11 2022 wuzx<wuzx1226@qq.com> - 0.13.0-4
- add sw64 patch

* Mon Mar 28 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 0.13.0-3
- delete old so file

* Thu Dec 16 2021 zhouwenpei <zhouwenpei1@huawei.com> - 0.13.0-2
- fix build error

* Mon Nov 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 0.13.0-1
- upgrade to 0.13.0

* Tue Jul 21 2020 jinzhimin <jinzhimin2@huawei.com> - 0.12.1-1
- upgrade to 0.12.1

* Mon Jun 29 2020 xinghe <xinghe1@huawei.com> - 0.10.1-10
- Type:bugfix
- ID:NA
- SUG:restart
- DESC: Change the patch name

* Sun Jun 28 2020 xinghe <xinghe1@huawei.com> - 0.10.1-9
- Type:bugfix
- ID:NA
- SUG:restart
- DESC: Shield regtest

* Wed Jun 24 2020 xinghe <xinghe1@huawei.com> - 0.10.1-8
- Type:bugfix
- ID:NA
- SUG:restart
- DESC: Remove the benchmarks from the regtest.

* Wed Jan 8 2020 hewenliang<hewenliang4@huawei.com> - 0.10.1-7
- Type:bugfix
- ID:NA
- SUG:restart
- DESC: Enable benchmarks in regtest.

* Mon Aug 12 2019 hewenliang<hewenliang4@huawei.com> - 0.10.1-6
- Type:bugfix
- ID:NA
- SUG:restart
- DESC: fix performance issue of the urcu.

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.10.1-5
- Package init
